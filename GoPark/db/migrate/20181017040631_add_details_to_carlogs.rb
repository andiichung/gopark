class AddDetailsToCarlogs < ActiveRecord::Migration[5.2]
  def change
    add_column :carlogs, :brand, :string
    add_column :carlogs, :color, :string
    add_column :carlogs, :time_in, :datetime
    add_column :carlogs, :time_out, :datetime
    add_column :carlogs, :created_at, :datatime
    add_column :carlogs, :updated_at, :datatime
  end
end
