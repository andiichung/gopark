# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_10_17_040631) do

  create_table "carlogs", force: :cascade do |t|
    t.string "plate_number"
    t.string "brand"
    t.string "color"
    t.datetime "time_in"
    t.datetime "time_out"
    t.time "created_at"
    t.time "updated_at"
  end

end
