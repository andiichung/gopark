class CarLog < ActiveRecord::Base
  validates :plate_number, presence:true, length: { minimum:4, maximum: 8}
  validates :time_in, presence:true
  validates :time_out, presence:true
  validates :color, presence:true
  validates :brand, presence:true
  self.table_name = "carlogs"
  # length {minimum: , maximum: ,in: 6..20, is: 6}
end
