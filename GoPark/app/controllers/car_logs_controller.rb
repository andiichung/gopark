class CarLogsController < ApplicationController
  def new
    @carlogs = CarLog.new
  end

  def create
    @carlogs = CarLog.new(carlogs_params)
    if @carlogs.save
        redirect_to car_log_path(@carlogs), notice: "You have successfully registered!"
    else
        render "new"
    end
  end

  def show
    if CarLog.all.count.zero?
      redirect_to root_path, alert: "There are no registered cars yet!"
    else
      @carlogs = CarLog.last
    end
  end

  private
    def carlogs_params
      params.require(:car_log).permit(:plate_number, :time_in, :time_out, :color, :brand, :created_at, :updated_at)
    end
end
